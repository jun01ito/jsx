# PSD→text指定レイヤー名すべて→csv || csv→text指定レイヤー名すべて→PSDのjsx書いてみた
PSDデータをテキストの最新で、、、ってやめよう!

## 動作確認環境
photoshop cs4
photoshop cs6
多分cs4以上ならwin mac どっちでもokだと思います。

## 使えそうなシーン
+ Title01
    + text01
+ Title02
    + text02
+ Title03
    + text03
+ Title04
    + text04

のような良くあるパターンのテキストデータを取り出したい
または、そこを編集したい場合に使用できそう

### 注意点
+ csvがShift_JISで保存されるので Sublime Text とか使ってる人でプラグイン入れてないと日本語文字化けで焦るので注意
+ csvに吐き出すテキストレイヤーは第一階層に配置して、それ以外のレイヤーは一度グループ化してください。
+ Photoshop Layer ex.)
    + text01
    + text02
    + text03
    + text04
    + [  ] ALL (それ以外のレイヤーを一旦グループ化 !!!　Header/Contents/Footer)
+ 使い方間違えたり、PCのパフォーマンス落ちてると、photoshop落ちるかも。ファイルの末尾に達しました。。。。にならないためにも、念には念をでバックアップとってからにしてください。
※ 使用は自己責任でお願いします(´・ω・｀)

### export.jsx
```jsx:export.jsx
//---------------------------------------
// レイヤー名をテキストに保存
//---------------------------------------
var CR = String.fromCharCode(13);
var savename = File.saveDialog("csvを保存する");
if (savename) {
    var fileObj = new File(savename);
    var flag = fileObj.open("w");
    if (flag == true) {
        writeLayerName(activeDocument);
        fileObj.close();
    } else {
        alert("ファイルが開けませんでした");
    }
}
//---------------------------------------
// 指定のテキストレイヤー名をｃｓｖに書き出し
//---------------------------------------
function writeLayerName(layObj) {
    var _title = 'title';
    var str = prompt("エクスポートするレイヤー名", _title);
    //var str = _title;
    var regObj = new RegExp(str, "g");
    var txtObj = activeDocument.artLayers;
    var n = layObj.artLayers.length;
    for (var i = 0; i < n; i++) {
        if (txtObj[i].kind == LayerKind.TEXT) {
            var txt = txtObj[i].textItem.contents;
            var layName = layObj.artLayers[i].name;
            var result = layName.match(regObj);
            if (result) {
                fileObj.write(txt+','+CR);
            }
        }
    }
alert("Done!!!");
}
```

#### export.jsxの使い方
+ 注意点を確認して、export.jsを実行!
+ csv保存のダイアログが出るので[fileName].csv保存

## Done!!!


### import.jsx

```jsx:import.jsx
//---------------------------------------
// csvのPATH
//---------------------------------------
//var filename = "C:/Users/ito/Desktop/jsx/text/text.csv";
var filename = "/Users/itoujunichi/Desktop/jsx/text/text.csv";
var fileObj = new File(filename);
var flg = fileObj.open("r");
var txtAry = [];
var resAry = [];
//---------------------------------------
// ｃｓｖファイル読み込み
//---------------------------------------
if (flg == true) {
    alert(filename+"を読み込みます");
    var textCsv = fileObj.read();
    csvCount = textCsv.split(",");
    for (var i = 0; i <= csvCount.length-2; i++) {
        alert(csvCount[i]);
        txtAry.push(csvCount[i]);
    };
    fileObj.close();
    //---------------------------------------
    // 指定のテキストレイヤー名判定
    //---------------------------------------
    function loadLayerName(layObj) {
        var _title = 'Title';
        var str = prompt("インポート先のレイヤー名", _title);
        var regObj = new RegExp(str, "g");
        var txtObj = activeDocument.artLayers;
        var n = layObj.artLayers.length;
        for (var i = 0; i < n; i++) {
            if (txtObj[i].kind == LayerKind.TEXT) {
                var txt = txtObj[i].textItem.contents;
                var layName = layObj.artLayers[i].name;
                var result = layName.match(regObj);
                if (result) {
                    //alert(layObj.artLayers[i]);
                    resAry.push(layObj.artLayers[i]);
                }
            }
        }
        //---------------------------------------
        // 指定のテキストレイヤー名をcsvに変更
        //---------------------------------------
        for (var i = 0; i < resAry.length; i++){
                resAry[i].textItem.contents = txtAry[i];
        }
        alert("Done!!!");
    }
    loadLayerName(activeDocument);
} else {
    alert("ファイルが開けませんでした");
}
```

#### import.jsxの使い方
+ import.jsxのfilenameにPathを通す。ターミナルにフォルダドラッグすればすぐ出ますね
+ 先ほどのcsvで変更した箇所を修正
+ inport.jsxを実行!

## Done!!!

## 結果
gruntを使ったときのような、「お〜っ! 働いてる働いてる。」感がある
落ちてるjsxしか使ったこと無かったのですが、これも50行くらいなのでいろいろ作りたくなる

デザイナーだって自動化したい!!! |ω・)ﾁﾗ

#### 参考
[Photoshop CS自動化作戦](http://www.openspc2.org/book/PhotoshopCS/)
[Adobe Photoshop Scripting](http://www.adobe.com/jp/devnet/photoshop/scripting.html)


